package com.example.arreglosjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //Objetos para relacionarlos con las vistas
    EditText texto;
    EditText multilinea;
    Button boton;

    //Arreglo
    String textos[];

    int contador=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textos=new String[4];

        //Relacionar nuestro objetos con los objetos del layout
        boton=findViewById(R.id.boton);
        texto=findViewById(R.id.texto);
        multilinea=findViewById(R.id.multilinea);
        multilinea.setText("");
        boton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (contador<4) {

            //1. Tomar el texto que está en el objeto texto
            String valor = texto.getText().toString();
            //2. Guardar valor en el arreglo
            textos[contador] = valor;
            contador++;
            texto.setText("");

        }else{

            multilinea.setText("");

            //Cuando el contador llegue a 4 entonces mostrar el arreglo en la multilínea
            for(int i=0; i <textos.length;i++){
                multilinea.append(textos[i]+"\n");
            }

        }

    }
}